package main

import (
	"bitbucket.org/Victor-Fed/rusprofile/internal/parser"
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"

	rusprofilepb "bitbucket.org/Victor-Fed/rusprofile/proto/rusprofile"
)

const (
	addressGRPC = ":8080"
	addressREST = ":8090"
)

type server struct {
	rusprofilepb.UnimplementedGreeterServer
}

func NewServer() *server {
	return &server{}
}

func (s *server) GetCompanyInfo(ctx context.Context, in *rusprofilepb.RequestBody) (*rusprofilepb.ResponseBody, error) {
	resp := parser.Run(in.INN)
	return &rusprofilepb.ResponseBody{Name: resp.Name, INN: in.INN, KPP: resp.Kpp, FIO: resp.Fio}, nil
}

func main() {
	// Create a listener on TCP port
	lis, err := net.Listen("tcp", addressGRPC)
	if err != nil {
		log.Fatalln("Failed to listen:", err)
	}

	// Create a gRPC server object
	s := grpc.NewServer()
	// Attach the Greeter service to the server
	rusprofilepb.RegisterGreeterServer(s, &server{})
	// Serve gRPC server
	log.Println("Serving gRPC on 0.0.0.0" + addressGRPC)
	go func() {
		log.Fatalln(s.Serve(lis))
	}()

	// Create a client connection to the gRPC server we just started
	// This is where the gRPC-Gateway proxies the requests
	conn, err := grpc.DialContext(
		context.Background(),
		"0.0.0.0:8080",
		grpc.WithBlock(),
		grpc.WithInsecure(),
	)
	if err != nil {
		log.Fatalln("Failed to dial server:", err)
	}

	gwmux := runtime.NewServeMux()
	// Register Greeter
	err = rusprofilepb.RegisterGreeterHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}

	gwServer := &http.Server{
		Addr:    addressREST,
		Handler: gwmux,
	}

	log.Println("Serving gRPC-Gateway on http://0.0.0.0" + addressREST)
	log.Fatalln(gwServer.ListenAndServe())
}
