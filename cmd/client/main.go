package main

import (
	"context"
	"log"
	"time"

	rusprofilepb "bitbucket.org/Victor-Fed/rusprofile/proto/rusprofile"
	"google.golang.org/grpc"
)

const (
	address    = "localhost:8080"
	defaultInn = 7713793524
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := rusprofilepb.NewGreeterClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.GetCompanyInfo(ctx, &rusprofilepb.RequestBody{INN: defaultInn})
	if err != nil {
		log.Fatalf("Could not get response: %v", err)
	}
	log.Printf("Response: %s", r)
}
