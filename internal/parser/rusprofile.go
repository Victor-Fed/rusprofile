package parser

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"strconv"
)

const baseUrl = "https://www.rusprofile.ru/search?search_inactive=0&query="

type Info struct {
	Name string
	Kpp  int64
	Fio  string
}

func Run(inn int64) *Info {
	resp, _ := http.Get(baseUrl + strconv.Itoa(int(inn)))
	info := &Info{}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		info.Kpp, _ = strconv.ParseInt(doc.Find("#clip_kpp").Text(), 10, 64)
		info.Fio = doc.Find(".hidden-parent .company-info__text").Text()
		info.Name = doc.Find(".leftcol .company-name").Text()
	}
	return info
}
