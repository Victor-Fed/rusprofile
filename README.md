# Тестовое rusprofile

- Необходимо сделать gRPC обёртку над сайтом https://www.rusprofile.ru/

- API Сервис должен реализовывать один метод, принимающий на вход ИНН компании, ищущий компанию на rusprofile, и возвращающий её ИНН, КПП, название, ФИО руководителя

## Порядок установки grpc (подробнее https://grpc-ecosystem.github.io/grpc-gateway/docs/tutorials/introduction/)
+ **Установить protobuf (https://grpc.io/docs/protoc-installation/)** 
+ **-Устанавливаем пакеты** 
    + ``` go get github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway ```
    ``` go get google.golang.org/protobuf/cmd/protoc-gen-go ```
    ``` go get google.golang.org/grpc/cmd/protoc-gen-go-grpc ```
    + Инициализируем модуль ``` go mod init ```
+ **Создаем файл .proto**
+ **Далее есть две альтернативы для генерации - либо через buf, либо через protoc, делаем через protoc:**
    + ``` protoc -I ./proto --go_out ./proto --go_opt paths=source_relative --go-grpc_out ./proto --go-grpc_opt paths=source_relative ./proto/rusprofile/server.proto```
    + Для генерации с gateway:
        + Делаем копию google/api https://github.com/googleapis/googleapis 
         + ``` protoc -I ./proto --go_out ./proto --go_opt paths=source_relative --go-grpc_out ./proto --go-grpc_opt paths=source_relative --grpc-gateway_out ./proto --grpc-gateway_opt paths=source_relative ./proto/rusprofile/server.proto ```

+ В итоге должны сгенериться следующие файлы: *.pb.go;  *_grpc.pb.go; *.pb.gw.go (для gateway)

## Swagger
- Генерация: ``` protoc -I ./proto --openapiv2_out ./gen/openapiv2   --openapiv2_opt logtostderr=true  ./proto/rusprofile/server.proto ```

### Запуск черех docker:

- ``` docker-compose up -d ```
- ``` docker-compose exec grpc bash ```
- ``` go build -o server cmd/server/main.go ```
- ``` go build -o client cmd/client/main.go ```
- ``` ./server ```
- ``` ./client ```