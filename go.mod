module bitbucket.org/Victor-Fed/rusprofile

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/swag v1.7.1 // indirect
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.5 // indirect
	google.golang.org/genproto v0.0.0-20210617175327-b9e0b3197ced
	google.golang.org/grpc v1.38.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/grpc/examples v0.0.0-20210514222045-a12250e98f97 // indirect
	google.golang.org/protobuf v1.26.0
)
